
public class DoppelLinkedListSort {
	Node head;
	Node tail;
	
	public DoppelLinkedListSort()
	{
		head = null;
		tail = null;
	}
	
	public void add(int i)
	{
		Node n = new Node(i);
		
		if(head == null)
		{
			head = n;
		}
		else
		{
			Node nLauf;
			nLauf = head;
			while(nLauf.hasNext())
			{
				nLauf = nLauf.getNext();
			}
			nLauf.setNext(n);
			n.setPrevious(nLauf);
			tail = n;
			
		}
	}

	public void print()
	{
		Node nlauf = head;
		do
		{
			System.out.println(nlauf.getValue());
			nlauf = nlauf.getNext();
		}
		while(nlauf != null);
		nlauf = tail;
		/*
		do
		{
			
			System.out.println(nlauf.getValue());
			nlauf = nlauf.getPrevious();
			
		}while(nlauf != null);
		*/
	}
	
	public void sort()
	{
		int i = head.getValue();
		//Node nLauf = new Node(0);
		Node nlauf = head;
		do
		{
			if(nlauf.getValue() < i)
			{
				head = nlauf;
				i = head.getValue();
			}
			nlauf = nlauf.getNext();
		}while(nlauf != null);
	}
}