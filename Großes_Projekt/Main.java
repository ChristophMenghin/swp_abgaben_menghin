
import java.io.IOException;
import java.util.Scanner;

public class Main {
	static Project project = new Project();
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) throws ClassNotFoundException, IOException {

		while (true) {
			System.out.println();
			System.out.println("Bitte w�hlen Sie eine Operation aus:");
			System.out.println();
			System.out.println("Aufgabe erstellen: 1");
			System.out.println("Aufgabe l�schen: 2");
			System.out.println("Aufgabe bearbeiten: 3");
			System.out.println("Alle Aufgaben auflistn: 4");
			System.out.println("Speichern: 5");
			System.out.println("Laden: 6");
			System.out.println();

			String st = sc.next();

			switch (st) {
			case "1":
				createTask();
				break;

			case "2":
				removeTask();
				break;

			case "3":
				editTask();
				break;

			case "4":
				showAllTasks();
				break;

			case "5":
				project.save();
				break;

			case "6":
				project.load();
				break;
			}
		}
	}

	public static void createTask() {
		String scha�123;
		String name;
		String description;
		int timePlanned;
		int timeReal;

		System.out.print("Bitte den Namen der Aufgabe eingeben: ");
		scha�123 = sc.nextLine();
		name = sc.nextLine();
		System.out.print("Bitte die Beschreibung der Aufgabe eingeben: ");
		description = sc.nextLine();
		System.out.print("Bitte die geplante Zeit eingeben: ");
		timePlanned = sc.nextInt();
		System.out.print("Bitte die reale Zeit eingeben: ");
		timeReal = sc.nextInt();
		project.addTask(name, description, timePlanned, timeReal);
	}

	public static void removeTask() {
		int index;
		System.out.println("Bitte die Aufgabennummer eingeben: ");
		index = sc.nextInt();
		project.deleteTask(index);
	}

	public static void editTask() {
		String name;
		String description;
		String timePlanned;
        String timeReal;
		int index;

		System.out.println("Bitte die Aufgabennummer eingeben: ");
		index = sc.nextInt();
		System.out.println("Bitte den Namen der Aufgabe eingeben: ");
		name = sc.nextLine();
		System.out.println("Bitte die Beschreibung der Aufgabe eingeben: ");
		description = sc.nextLine();
		System.out.println("Bitte die geplante Zeit eingeben: ");
		timePlanned = sc.nextLine();
		System.out.println("Bitte die reale Zeit eingeben: ");
		timeReal = sc.next();

		project.editTask(index, name, description, timePlanned, timeReal);

	}

	public static void showAllTasks() {

		System.out.println(
				"Aufgabennummer \t" + "Aufgabenname" + "\t" + "Aufgabenbeschreibung \t" + "Reale Zeit \t" + "Geplante Zeit \t" + "Zeitdifferenz");
		System.out.println("--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		for (Task t : project.getArr()) {
			System.out.print(t.getTaskNumber() + "\t        " + t.getTaskName() + "\t        " + t.getTaskDescription()
					+ "\t                     " + t.getTimeReal() + "\t               " + t.getTimePlanned() + "\t          " + t.getTimeDiff());
			System.out.println();

		}
	}
}
