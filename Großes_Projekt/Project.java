
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class Project implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<Task> taskList = new ArrayList<Task>();

	public Project() {

	}

	public void addTask(String taskName, String taskDescription, int timePlanned, int timeReal) {
		Task t = new Task(taskName, taskDescription, timePlanned, timeReal);
		if (taskList.size() > 0)
		{
		int index = taskList.get(taskList.size()-1).getTaskNumber();
		t.setTaskNumber(index + 1);
		}
		taskList.add(t);
	}

	public void deleteTask(int taskNumber) {
		taskList.remove(taskNumber);
	}

	public void editTask(int num, String name, String description, String timePlanned, String timeReal) {
		if (!(name.equals(""))) {
			for (Task t : taskList) {
				if (t.getTaskNumber() == num) {
					t.setTaskName(name);
				}
			}
		}
		if (!(description.equals(""))) {
			for (Task t : taskList) {
				if (t.getTaskNumber() == num) {
					t.setTaskDescription(description);
				}
			}
		}
		if (!(timePlanned.equals(""))) {
			for (Task t : taskList) {
				if (t.getTaskNumber() == num) {
					t.setTimePlanned(Integer.parseInt(timePlanned));
				}
			}
		}
		if (!(timeReal.equals(""))) {
			for (Task t : taskList) {
				if (t.getTaskNumber() == num) {
					t.setTimeReal(Integer.parseInt(timeReal));
				}
			}
		}
	}
	
	public ArrayList<Task> getArr()
	{
		return taskList;
	}

	public Task getTask(int taskNum) {
		return taskList.get(taskNum);
	}

	public void save() {
		try {
			FileOutputStream fileOut = new FileOutputStream("Savefile");
			ObjectOutputStream out;
			try {
				out = new ObjectOutputStream(fileOut);
				out.writeObject(taskList);
				out.close();
				fileOut.close();

			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public void load() throws IOException, ClassNotFoundException {
		FileInputStream fileIn = new FileInputStream("Savefile");
		ObjectInputStream objin = new ObjectInputStream(fileIn);

		Object obj = objin.readObject();
		taskList = (ArrayList<Task>) obj;

		objin.close();

	}

}
