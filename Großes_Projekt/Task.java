
import java.io.Serializable;

public class Task implements Serializable {
	private static final long serialVersionUID = 1L;

	private int taskNumber;
	private String taskName;
	private String taskDescription;
	private int timePlanned;
	private int timeReal;
	private int timeDiff;

	public Task(String taskName, String taskDescription, int timePlanned, int timeReal) {
		super();
		this.taskName = taskName;
		this.taskDescription = taskDescription;
		this.timePlanned = timePlanned;
		this.timeReal = timeReal;
		timeDiff = timePlanned - timeReal;
	}

	public int getTaskNumber() {
		return taskNumber;
	}

	public void setTaskNumber(int taskNumber) {
		this.taskNumber = taskNumber;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getTaskDescription() {
		return taskDescription;
	}

	public void setTaskDescription(String taskDescription) {
		this.taskDescription = taskDescription;
	}

	public int getTimePlanned() {
		return timePlanned;
	}

	public void setTimePlanned(int timePlanned) {
		this.timePlanned = timePlanned;
	}

	public int getTimeReal() {
		return timeReal;
	}

	public void setTimeReal(int timeReal) {
		this.timeReal = timeReal;
	}


	public int getTimeDiff() {
		timeDiff = timePlanned - timeReal;
		return timeDiff;
	}


}
