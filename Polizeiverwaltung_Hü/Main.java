
import java.util.ArrayList;

public class Main {

	ArrayList<Fahrzeug> polizeifuhrpark = new ArrayList<Fahrzeug>();
	ArrayList<Fahrzeug> ausgeliehen = new ArrayList<Fahrzeug>();

	public Main() {
		
		polizeifuhrpark.add(new Motorrad(2015, "BMW", "schwarz", "Motorrad", 1200));
		polizeifuhrpark.add(new Motorrad(2016, "BMW", "schwarz", "Motorrad", 1200));
		polizeifuhrpark.add(new PKW(2016, "Skoda", "silber","PKW", "Nagelstreifen"));
		polizeifuhrpark.add(new PKW(2015, "Audi", "schwarz", "PKW", "Anhängerkupplung"));
		polizeifuhrpark.add(new Transporter(2010, "Mercedes", "silber", "Transporter", 3, 10));
		polizeifuhrpark.add(new Transporter(2010, "VW", "silber", "Transporter", 9, 0));

		Motorrad meinMotorrad = (Motorrad) 
		ausleihen("Motorrad");
		ausleihen("Motorrad");
		ausleihen("Motorrad");
		zurueckgeben(meinMotorrad);

	}

	public static void main(String[] args) {
		new Main();
	}

	private Fahrzeug ausleihen(String typ) {
		for (int i = 0; i < polizeifuhrpark.size(); i++) {
			if (polizeifuhrpark.get(i).getTyp().equals(typ)) {
				ausgeliehen.add(polizeifuhrpark.get(i));
				polizeifuhrpark.remove(i);
				return ausgeliehen.get(ausgeliehen.size() - 1);
			}
		}
		System.out.println("Es ist kein gewünschtes Fahrzeug vorhanden");
		return null;
	}

	private void zurueckgeben(Fahrzeug f) {
		ausgeliehen.remove(f);
		polizeifuhrpark.add(f);
	}

}
