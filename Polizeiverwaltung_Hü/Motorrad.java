
public class Motorrad extends Fahrzeug{

	private int hubrau;

	public Motorrad(int baujahr, String hersteller, String farbe, String typ, int hubrau) {
		super(baujahr, hersteller, farbe, typ);
		this.hubrau = hubrau;
	}

	public int getHubrau() {
		return hubrau;
	}

	public void setHubrau(int hubrau) {
		this.hubrau = hubrau;
	}
	
	
}
