
public class Transporter extends Fahrzeug {

	private int sitzplaetze;
	private int ladeflaeche;

	public Transporter(int baujahr, String hersteller, String farbe, String typ, int sitzplaetze, int ladeflaeche) {
		super(baujahr, hersteller, farbe, typ);
		this.sitzplaetze = sitzplaetze;
		this.ladeflaeche = ladeflaeche;
	}

	public int getSitzplaetze() {
		return sitzplaetze;
	}

	public void setSitzplaetze(int sitzplaetze) {
		this.sitzplaetze = sitzplaetze;
	}

	public int getLadeflaeche() {
		return ladeflaeche;
	}

	public void setLadeflaeche(int ladeflaeche) {
		this.ladeflaeche = ladeflaeche;
	}
	
	
}
