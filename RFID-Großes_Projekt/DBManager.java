import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class DBManager {
	
private Connection c;
Scanner sc1 = new Scanner(System.in);
Scanner sc2 = new Scanner(System.in);
Scanner sc3 = new Scanner(System.in);
Scanner sc4 = new Scanner(System.in);
Scanner sc5 = new Scanner(System.in);

public DBManager() throws ClassNotFoundException
{
	try
	{
		Class.forName("org.sqlite.JDBC");
		c = DriverManager.getConnection("jdbc:sqlite:C:\\Sqlite\\Timetracker.db");
	}
	catch (SQLException e) {
		e.printStackTrace();
	}
}

private PreparedStatement executeUpdate() {
	return null;
}
public void createTimeTable() {
	PreparedStatement stmt;
	String create = "CREATE TABLE IF NOT EXISTS Time(" + "TimeID INT PRIMARY KEY, " + "StartTime TEXT NOT NULL, " + "EndTime TEXT NOT NULL, " + "FSTopicID INT REFERENCES Topic(TopicID));";
	try {
		stmt = c.prepareStatement(create);
		stmt.executeUpdate();
		stmt.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}
}
public void createTopicTable() {
	PreparedStatement stmt;
	String create = "CREATE TABLE IF NOT EXISTS Topic(" + "TopicID INT PRIMARY KEY, " + "Name TEXT NOT NULL, " + "RFID TEXT NOT NULL)";
	try {
		stmt = c.prepareStatement(create);
		stmt.executeUpdate();
		stmt.close();
	} catch (SQLException e) {
		e.printStackTrace();
	}
}
public void insertTopic() {
	System.out.println("TopicID eingeben.");
	int k = sc1.nextInt();
	System.out.println("Name eingeben");
	String n = sc2.nextLine();
	System.out.println("RFID eingeben");
	String r = sc3.nextLine();
	PreparedStatement stmt = null;
	try {
		String sql = "INSERT INTO Topic(TopicID, Name, RFID) VALUES(?,?,?)";
		stmt = c.prepareStatement(sql);
		stmt.setInt(1, k);
		stmt.setString(2, n);
		stmt.setString(3, r);
		stmt.executeUpdate();
	} catch (SQLException e) {
		e.printStackTrace();
	}
	System.out.println("Erfolgreich erstellt.");
	System.out.println("");
}
public void insertTime() {
	System.out.println("TimeID eingeben.");
	int i1 = sc1.nextInt();
	System.out.println("Startzeit eingeben.");
	String s1 = sc2.nextLine();
	System.out.println("Endzeit eingeben.");
	String s2 = sc3.nextLine();
	System.out.println("ID des Topics eingeben.");
	int i2 = sc4.nextInt();
	PreparedStatement stmt = null;
	try{
		String sql = "INSERT INTO Time(TimeID, StartTime, EndTime, FSTopicID) VALUES(?,?,?,?)";
		stmt = c.prepareStatement(sql);
		stmt.setInt(1,i1);
		stmt.setString(2, s1);
		stmt.setString(3, s2);
		stmt.setInt(4, i2);
		stmt.executeUpdate();
	} catch (SQLException e) {
		e.printStackTrace();
	}
	System.out.println("Erfolgreich erstellt.");
	System.out.println("");
	}
public void deleteTime()
{
	System.out.println("Geben sie die ID von der Time ein, die sie löschen wollen");
	int IDTime = sc1.nextInt();
	PreparedStatement stmt = null;
	String delete = "DELETE FROM TIME WHERE ID = " + IDTime;
	try{
		stmt = c.prepareStatement(delete);
		stmt.executeUpdate();
	}
	catch (SQLException e)
	{
		e.printStackTrace();
	}
}
public void deleteTopic()
{
	System.out.println("Geben sie die ID von des Topic ein, die sie löschen wollen");
	int i2 = sc1.nextInt();
	PreparedStatement stmt = null;
	String delete = "DELETE FROM Topic WHERE TopicID = " + i2;
	try{
		stmt = c.prepareStatement(delete);
		stmt.executeUpdate();
	}
	catch (SQLException e)
	{
		e.printStackTrace();
	}
}
public void updateTime() {
	System.out.println("Geben sie die ID von der Time ein, die sie updaten wollen");
	int ID = sc1.nextInt();
	System.out.println("neue StartTime?");
	String neueStartTime = sc2.nextLine();
	System.out.println("neue EndTime?");
	String neueEndTime = sc3.nextLine();
	System.out.println("neue TopicID eingeben.");
	int NuerFremdschlüssel = sc4.nextInt();
	try{
		Statement stmt = c.createStatement();
		String sql = "UPDATE TIME " + "SET StartTime="+ neueStartTime + ", EndTime=" + neueEndTime + ", TopicID=" + NuerFremdschlüssel +  "WHERE TimeID=" + ID;
		stmt.executeUpdate(sql);
	}
	catch(SQLException e) {
		e.printStackTrace();
	}
	System.out.println("");
}
public void updateTopic() {
	System.out.println("Geben sie die ID von des Topic ein, die sie updaten wollen");
	int ID = sc1.nextInt();
	System.out.println("neuen Namen eingeben.");
	String neuerName = sc2.nextLine();
	System.out.println("neue RFID eingeben.");
	String neueRFID = sc3.nextLine();
	try{
		Statement stmt = c.createStatement();
		String sql = "UPDATE TOPIC " + "SET Name="+ neuerName + ", RFID=" + neueRFID + "WHERE TimeID=" + ID;
		stmt.executeUpdate(sql);
	}
	catch(SQLException e) {
		e.printStackTrace();
	}
	System.out.println("");
}
public void showTime() {
	System.out.println("Times:");
	String show = "SELECT * FROM Time";
	try {
		Statement stmt = c.createStatement();
		ResultSet rs = stmt.executeQuery(show);
		while (rs.next()) {
			System.out.println(rs.getInt(1) + "-" + rs.getString(2) + "-" + rs.getString(3) + "-" + rs.getInt(4) );
		}
	} catch (SQLException e) {
		e.printStackTrace();
	}
	System.out.println("");
}
public void showTopic() {
	System.out.println("Topics:");
	String show = "SELECT * FROM Topic";
	try {
		Statement stmt = c.createStatement();
		ResultSet rs = stmt.executeQuery(show);
		while (rs.next()) {
			System.out.println(rs.getInt(1) + "-" + rs.getString(2) + "-" + rs.getString(3));
		}
	} catch (SQLException e) {
		e.printStackTrace();
	}
	System.out.println("");
}
}
