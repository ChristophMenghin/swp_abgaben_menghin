import java.util.Scanner;


public class TimeTaker_Main {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) throws ClassNotFoundException {
		DBManager db = new DBManager();
		db.createTimeTable();
		db.createTopicTable();
		while (true) {
			System.out.println("Was wollen sie tun?");
			System.out.println("1-Times anzeigen lassen");
			System.out.println("2-Topics anzeigen lassen");
			System.out.println("3-Time erstellen");
			System.out.println("4-Topic erstellen");
			System.out.println("5-Time l�schen");
			System.out.println("6-Topic l�schen");
			System.out.println("7-Time updaten");
			System.out.println("8-Topic updaten");
			String st = sc.next();
			switch (st) {
			case "1":
				db.showTime();
				break;
				
			case "2":
				db.showTopic();
				break;
				
			case "3":
				db.insertTime();
				break;

			case "4":
				db.insertTopic();
				break;

			case "5":
				db.deleteTime();
				
			case "6":
				db.deleteTopic();
				break;
			
			case "7":
				db.updateTime();
				break;
			
			case "8":
				db.updateTopic();
			}
		}

	}

}
