
public class Main {

	public static void main(String[] args) {

		int[] arr = {1,2,3,4,5,6,9,8,7};
		int[] arr2 = {8,24,3,4,5,6,9,8,7,9,4,7,234,56};
		
		int[] unsortiert = { 4, 1, 8, -3, 5, 7 };
		int[] sortiert = insertionSort(unsortiert);

		for (int i = 0; i < sortiert.length; i++) {
			System.out.print(sortiert[i] + ", ");
		}
		System.out.println();
		  
		bubblesort(arr);
		bubblesort(arr2);
	}

	public static void bubblesort(int[] zuBubbleSortierenderArray) {
		int zwischenspeicher;
		int arrayverkleinern = 0;
		for (int i = 0; i < zuBubbleSortierenderArray.length; i++)
		{
			arrayverkleinern++;
			for(int j = 0; j < zuBubbleSortierenderArray.length - arrayverkleinern; j++ )
			{
				if(zuBubbleSortierenderArray[j]>zuBubbleSortierenderArray[j+1])
				{
					zwischenspeicher = zuBubbleSortierenderArray[j];
					zuBubbleSortierenderArray[j] = zuBubbleSortierenderArray[j+1];
					zuBubbleSortierenderArray[j+1] = zwischenspeicher;
					
				}	
			}
		}
		for(int k = 0; k < zuBubbleSortierenderArray.length; k++)
		{
			System.out.print(zuBubbleSortierenderArray[k] + ", ");
		}
		System.out.println();
	}
	public static int[] insertionSort(int[] sortieren) {
		int zwischenSpeicher;
		for (int i = 1; i < sortieren.length; i++) {
			zwischenSpeicher = sortieren[i];
			int j = i;
			while (j > 0 && sortieren[j - 1] > zwischenSpeicher) {
				sortieren[j] = sortieren[j - 1];
				j--;
			}
			sortieren[j] = zwischenSpeicher;
		}
		return sortieren;
	}
	
}


