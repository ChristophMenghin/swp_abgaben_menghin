package Calc_package;
import static org.junit.Assert.*;

import org.junit.Test;

import Test_package.Mycalc;

public class MycalcTest {

	@Test
	public void testadd() {
		assertEquals(10, Mycalc.add(3,7), 0.0001);
	}
	@Test
	public void testsub() {
		assertEquals(2, Mycalc.sub(7,5), 0.0001);
	}
	@Test
	public void testmult() {
		assertEquals(18, Mycalc.mult(6,3), 0.0001);
	}
	@Test
	public void testdiv() {
		assertEquals(2, Mycalc.div(6,3), 0.0001);
	}
	@Test
	public void testpot() {
		assertEquals(36, Mycalc.pot(6), 0.0001);
	}
	@Test
	public void testlog() {
		assertEquals(1, Mycalc.log(10), 0.0001);
	}

}
